import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { InfoPageComponent } from './pages/info/info.page.component';
import { HomePageComponent } from './pages/home/home.page.component';
import { CenterLayoutComponent } from './layouts/center/center.layout.component';
import { NavBarElementComponent } from './components/nav-bar/nav-bar-element/nav-bar-element.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    InfoPageComponent,
    HomePageComponent,
    CenterLayoutComponent,
    NavBarElementComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
