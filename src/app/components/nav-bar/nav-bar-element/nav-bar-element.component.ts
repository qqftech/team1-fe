import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-nav-bar-element',
  template: `
    <a
      [routerLink]="link"
      routerLinkActive="selected"
      [routerLinkActiveOptions]="{ exact: true }"
      ><ng-content></ng-content
    ></a>
  `,
  styles: [
    `
      a {
        text-decoration: none;
        color: black;
      }

      .selected {
        background-color: #baecec;
        border: solid black 1px;
        padding: 0.2rem 0.5rem;
      }
    `,
  ],
})
export class NavBarElementComponent {
  @Input() link!: string;
}
