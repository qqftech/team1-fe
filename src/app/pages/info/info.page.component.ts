import { Component, OnInit } from '@angular/core';
import { ServerStatusService } from 'src/app/core/service/server-status.service';

@Component({
  templateUrl: './info.page.component.html',
  styleUrls: ['./info.page.component.scss'],
})
export class InfoPageComponent implements OnInit {
  serverStatus = "Cargando...";

  constructor(private serverStatusService: ServerStatusService) {}

  ngOnInit() {
    this.serverStatusService.getStatus().subscribe({
      next: (statusResponse) => {
        this.serverStatus = statusResponse.status;
      },
      error: (err => {
        this.serverStatus = "El servidor ha fallado";
      })
    });
  }
}
